import React, { Component } from 'react';
import { StyleSheet, View, StatusBar, ImageBackground } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';

import LoaderModal from '../../utils/components/modal_loader';

class loadingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal:true,
      textModal: 'Cargando...'
    };
  }

  async componentDidMount() {
    setTimeout(() => {
      this.props.dispatch(
        NavigationActions.navigate({
            routeName: 'homeContainer'
        })
      )
    }, 3000);
  }

  render() {
    return (
      <View style={styles.container} radius={300}>
        <StatusBar
          backgroundColor="#26828B"
          barStyle="light-content"
        />
        <LoaderModal visibleModal={this.state.visibleModal} text={this.state.textModal} />
        <ImageBackground
          source={require('../../assets/backsplash.png')}
          style={styles.imgBck}>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#26828B'
  },
  imgBck: {
    resizeMode: 'contain',
    height: '100%',
    width: '100%'
},
  activityIndicator:
  {
    bottom: 0,
    position: 'absolute',
  }
})


const mapStateToProps = (state) => {
  return {

  }
}

//make this component available to the app
export default connect(mapStateToProps)(loadingComponent);

