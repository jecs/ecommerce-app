import React, { Component } from 'react';
import { View, Text, StatusBar } from 'react-native';

class homeContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View>
        <StatusBar
          backgroundColor="#26828B"
          barStyle="light-content"
        />
        <Text> home_container </Text>
      </View>
    );
  }
}

export default homeContainer;
