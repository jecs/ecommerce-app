import {createSwitchNavigator } from 'react-navigation';
import React from 'react'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { fromLeft, fromBottom } from 'react-navigation-transitions';

import Loading from '../../models/loading/loading_container';
import homeContainer from '../../models/home/home_container';

const switchNavigator = createSwitchNavigator({
    Loading: Loading,
    homeContainer:homeContainer
},  {
        initialRouteName: 'Loading',
        transitionConfig: () => fromLeft(),
    }
)

export default switchNavigator;