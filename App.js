import React, {Component} from 'react';
import {AppState} from 'react-native'
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from './src/helpers/utils/store'
import {StyleSheet} from 'react-native';

import AppNavigatorWithState from './src/helpers/navigation/app-navigator-with-state'
import SplashScreen from 'react-native-splash-screen';

type Props = {};
export default class App extends Component<Props> {

  state = {
    appState: AppState.currentState
  }

  componentDidMount() {
    SplashScreen.hide()
  }
  
  render() {
    return (    
      <Provider store={store} >
        <PersistGate persistor={persistor} >
          <AppNavigatorWithState/>
        </PersistGate>
      </Provider>    
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
